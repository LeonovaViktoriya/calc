package calc;

import javax.xml.ws.Endpoint;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Endpoint.publish("http://localhost:8080/CalcEndPoint?wsdl", new CalcEndPoint());
    }
}
