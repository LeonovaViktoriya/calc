package calc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "calc.CalcEndPointService")
public class CalcEndPoint implements CalcEndPointService {
    @WebMethod
    public int sum(@WebParam(name = "a") int a, @WebParam(name = "b") int b ){
        return a+b;
    }
}
