package calc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-01-29T16:08:10.130+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://calc/", name = "CalcEndPointService")
@XmlSeeAlso({ObjectFactory.class})
public interface CalcEndPointService {

    @WebMethod
    @Action(input = "http://calc/CalcEndPointService/sumRequest", output = "http://calc/CalcEndPointService/sumResponse")
    @RequestWrapper(localName = "sum", targetNamespace = "http://calc/", className = "calc.Sum")
    @ResponseWrapper(localName = "sumResponse", targetNamespace = "http://calc/", className = "calc.SumResponse")
    @WebResult(name = "return", targetNamespace = "")
    public int sum(
        @WebParam(name = "a", targetNamespace = "")
        int a,
        @WebParam(name = "b", targetNamespace = "")
        int b
    );
}
